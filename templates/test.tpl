<title>Test</title>
<ul>
{% for user, url in users.items() %}
  <li><a href="{{ url }}">{{ user }}</a></li>
{% endfor %}
</ul>

# vim: filetype=jinja
