#!/usr/bin/env python3

from bottle import route, run, jinja2_view as view, jinja2_template as template, TEMPLATE_PATH

TEMPLATE_PATH.append('./templates/')

@route('/')
def index():
    users = {"bob": "https://test", "bill": "https://test2",}
    return template('test', users=users)

if __name__ == "__main__":
    run()
